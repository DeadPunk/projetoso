
# -*- coding: utf-8 -*-
'''aceitar caracteres especiais'''

'''
Neste projeto foi criada uma classe chamada FakeDatabase, para simular uma base de dados 
que possui um valor inicial 0 e esse valor é incrementado pela função locked_update,
que faz uso de threads para realizar essa tarefa
imortando bibliotecas
'''

import logging
import threading
import time
import concurrent.futures

#classe criada para simular uma base de dados 
class FakeDatabase:
    def __init__(self):
        self.value = 0
        self._lock = threading.Lock()

    def locked_update(self, name):
        logging.info("Thread %s: iniciando update", name)
        logging.debug("Thread %s prestes a dar lock", name) #retorna essa mensagem caso de erro
        
        #condição que inicia o lock
        with self._lock:
            logging.debug("Thread %s com lock", name)
            local_copy = self.value
            local_copy += 1
            self.value = local_copy
            logging.debug("Thread %s prestes a liberar o lock", name)
        logging.debug("Thread %s depois do lock", name)
        logging.info("Thread %s: terminando o update", name)

if __name__ == '__main__':

	format = "%(asctime)s: %(message)s"
	logging.basicConfig(format = format, level = logging.INFO,
	 					datefmt = "%H:%M:%S")

    #colocando a classe FakeDatabase em uma variavel 
	database = FakeDatabase()
	logging.info("Testando update. O valor inicial é %d.", database.value)

    #Pool para executar multiplas threads
	with concurrent.futures.ThreadPoolExecutor(max_workers = 2) as executor:
		for index in range(2):
			executor.submit(database.locked_update, index)
	logging.info("Testando update. Fim, o valor é %d.", database.value)
